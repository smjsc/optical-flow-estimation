import os

import h5py


if __name__ == '__main__':
    data_path = '/home/wei-liao/workspace/Datasets/optical_flow/training'
    data_names = sorted(os.listdir(data_path))
    for i, data_name in enumerate(data_names):
        with h5py.File(os.path.join(data_path, data_name), 'r') as f:
            a = 1
