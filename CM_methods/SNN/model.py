import torch
import torch.nn as nn
# from sinabs.layers import IAFSqueeze
try:
    from sinabs.exodus.layers import IAFSqueeze
except (ImportError, ModuleNotFoundError):
    from sinabs.layers import IAFSqueeze
    print('Not install sinabs-exodus')
else:
    print('Use sinabs-exodus for acceleration')

from sinabs.layers import IAFSqueeze

class SpikingFlowNet(nn.Module):
    def __init__(self, batch_size):
        super().__init__()
        self.batch_size = batch_size
        model_seq = [
            nn.Conv2d(in_channels=2, out_channels=16, kernel_size=3, stride=1, padding=1),
            IAFSqueeze(batch_size=self.batch_size, min_v_mem=-1),
            # Weight mem: 3*3*2*16=0.28 Ki, Neuron mem: 32*32*16=16 Ki,
            nn.Conv2d(in_channels=16, out_channels=32, kernel_size=3, stride=1, padding=1),
            # Weight mem 3*3*16*32=4.5 Ki, Neuron mem: 32*32*32=32 Ki
            IAFSqueeze(batch_size=self.batch_size, min_v_mem=-1),
            nn.Conv2d(in_channels=32, out_channels=32, kernel_size=3, stride=1, padding=1),
            # Weight mem 3*3*32*32=9 Ki, Neuron mem: 32*32*32=32Ki
            IAFSqueeze(batch_size=self.batch_size, min_v_mem=-1),
            nn.Conv2d(in_channels=32, out_channels=16, kernel_size=3, stride=1, padding=1),
            # Weight mem 3*3*32*16=4.5 Ki, Neuron mem: 32*32*16=16Ki
            IAFSqueeze(batch_size=self.batch_size, min_v_mem=-1),
            nn.Conv2d(in_channels=16, out_channels=8, kernel_size=1, stride=1, padding=1),
            # Weight mem 3*3*16*8=1.25 Ki, Neuron mem: 32*32*8=8Ki
            IAFSqueeze(batch_size=self.batch_size, min_v_mem=-1),
            nn.Conv2d(in_channels=8, out_channels=2, kernel_size=1, stride=1, padding=1),
            torch.nn.Tanh()
            # # Weight mem 3*3*8*2=0.28 Ki, Neuron mem: 32*32*2=2Ki
            # IAFSqueeze(batch_size=self.batch_size, min_v_mem=-1),
        ]
        self.model = nn.Sequential(*model_seq)

    def forward(self, x):
        y = self.model(x)
        y = torch.reshape(y, (self.batch_size, -1, y.size()[1], y.size()[2], y.size()[3]))

        return y



if __name__ == '__main__':
    model = SpikingFlowNet(batch_size=8)
    input = torch.rand((160, 2, 128, 128))
    output = model(input)
    print(output.size())