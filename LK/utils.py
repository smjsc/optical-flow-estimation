import os
import time

import cv2 as cv
import h5py
import numpy as np
import scipy.io
from sklearn.neighbors import KDTree
import torch

def distance_surface(events, h, w, map_ids):
    """
    Compute distance surface from events, the pixel value in distance surface is the Euclidean distance to the nearest
    event. We use KDTree to quickly compute the nearest event distance of each pixel.
    :param events: N*4 numpy array containing N events with [x, y, p, t]
    :param h, w: height and width of the sensor
    :param map_ids: x, y coordinates array with [N, 2] to store
    :return: distance surface
    """
    threshold = 13
    event_surf = torch.zeros((1, 1, h, w))
    event_surf[0, 0, events[:, 1].astype(np.int64), events[:, 0].astype(np.int64)] = 1
    weight = torch.ones((1, 1, 5, 5))
    res = torch.nn.functional.conv2d(event_surf, weight, padding=2)
    res[res<threshold] = 0
    leafs = int(events.shape[0]/2)
    inds = torch.where(res > 0)
    if inds[2].shape[0] > 1:
        kdtree = KDTree(np.array([inds[2].numpy(), inds[3].numpy()]).transpose(), leaf_size=leafs)
        distances, idxs = kdtree.query(map_ids, 1)
        distances = distances.reshape(h, w, order='F')
        distances = distances.squeeze()
    else:
        distances = np.zeros((h, w)) + 255
    return distances, res.squeeze().numpy(), event_surf.squeeze().numpy()


def distance_surface_v2(events, h, w):
    """
    Compute distance surface by cv.distanceTransform
    :param events: N*4 numpy array containing N events with [x, y, p, t]
    :param h, w: height and width of the sensor
    :return:
    """
    kernel_size = 3
    threshold = 4
    kernel = torch.ones((1, 1, kernel_size, kernel_size))
    event_surf = torch.zeros((1, 1, h, w))
    event_surf[0, 0, events[:, 1].astype(np.int64), events[:, 0].astype(np.int64)] = 1
    res = torch.nn.functional.conv2d(event_surf, kernel, padding=kernel_size//2)
    res = res.squeeze().numpy()
    _, res = cv.threshold(res, threshold, 255, cv.THRESH_BINARY_INV)
    dist_surf = cv.distanceTransform(res.astype(np.uint8), distanceType=cv.DIST_L2, maskSize=cv.DIST_MASK_PRECISE)
    return dist_surf, res, event_surf


def time_surface(events, h, w):
    """
    Compute time surface from events, the pixel value in time surface is normalized timestamp of the latest event
    in corresponding pixel coordinate.
    :param events: N*4 numpy array containing N events with [x, y, p, t]
    :return: time surface
    """
    events[:, 3] = (events[:, 3] - np.min(events[:, 3])) / (np.max(events[:, 3]) - np.min(events[:, 3]))
    surf = np.zeros((w, h))
    surf[events[:, 0].astype(np.int64), events[:, 1].astype(np.int64)] = events[:, 3]
    return surf


def vis_with_arrow(input_img, flow, freq=20):
    """
    :param input_img: event image with [h, w]
    :param flow: uv_flow with [w, w, 2]
    :return:
    """
    inds = np.where(input_img > 0)
    scale = 255
    for i in range(0, inds[0].shape[0], freq):
        y, x = int(inds[0][i]), int(inds[1][i])
        input_img = cv.arrowedLine(input_img, (x, y), (int(x+flow[y, x, 0]*scale), int(y+flow[y, x, 1]*scale)), (255, 255, 255), 1)
    return input_img


def get_events_from_mat(data_path):
    mat_data = scipy.io.loadmat(data_path)
    t = mat_data['events'][0, 0]['t']
    x = mat_data['events'][0, 0]['x']
    y = mat_data['events'][0, 0]['y']
    p = mat_data['events'][0, 0]['p']
    events = np.concatenate([x-1, y-1, t, p], axis=1)
    # events = np.array([x, y, p, t]).transpose()
    return events


def slice_events(ev_t, slice_time=0.01, slice_count=5000, slice_by_time=True):
    if slice_by_time:
        assert ev_t[-1] - ev_t[0] > slice_time
        ts = np.arange(ev_t[0], ev_t[-1], slice_time)
        inds = np.searchsorted(ev_t, ts)
    else:
        assert ev_t.shape[0] > slice_count
        inds = np.arange(0, ev_t.shape[0], slice_count)

    return inds.astype(np.int64)


if __name__ == '__main__':
    data_path = '/home/wei-liao/workspace/Datasets/optical_flow/HQF/desk.h5'
    f = h5py.File(data_path, 'r')
    events = f['events']
    x = np.array(events['xs'])
    y = np.array(events['ys'])
    p = np.array(events['ps'])
    t = np.array(events['ts'])
    events = np.array([x, y, p, t]).transpose()
    events = events[:2000, :]
    surface, indexs = distance_surface(events, w=180, h=240)