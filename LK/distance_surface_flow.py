import os
import time

import cv2 as cv
from cv2 import calcOpticalFlowFarneback
import flow_vis
import h5py
import numpy as np

from utils import *


if __name__ == '__main__':
    # data_path = '/home/liaowei/workspace/projects/datasets/optical_flow/HQF/desk.h5'
    # # tmp_path = '/home/liaowei/workspace/projects/tmp/distance_surface/'
    # f = h5py.File(data_path, 'r')
    # events = f['events']
    # x = np.array(events['xs'])
    # y = np.array(events['ys'])
    # p = np.array(events['ps'])
    # t = np.array(events['ts'])
    # events = np.array([x, y, p, t]).transpose()
    data_path = 'E://study//Distsurf Paper Codes//hand_events.mat'
    events = get_events_from_mat(data_path)
    inds = slice_events(events[:, 2])
    dist_surfaces = []  # store surfaces
    flows_hs = []
    event_surfaces = []
    h, w = 260, 346
    map_ys, map_xs = np.meshgrid(np.arange(0, h), np.arange(0, w))
    map_xs = map_xs.reshape(-1, 1)
    map_ys = map_ys.reshape(-1, 1)
    map_ids = np.concatenate([map_ys, map_xs], axis=1)
    # Horn_Schunck parameters
    hs_param = dict(pyr_scale=0.5, levels=3, winsize=15, iterations=3, poly_n=5, poly_sigma=1.1, flags=0)
    save_path = 'E://study///tmp//distance_surface//'
    os.makedirs(save_path, exist_ok=True)
    for i in range(100):
        print(i)
        dist_surface, event, event_raw = distance_surface_v2(events[inds[i]:inds[i+1], :], h=h, w=w)
        dist_surfaces.append(dist_surface)
        event_surfaces.append(event)
        cv.imwrite(os.path.join(save_path, 'distance_' + format(i, '05d') + '.png'), dist_surface/np.max(dist_surface)*255)
        cv.imwrite(os.path.join(save_path, format(i, '05d') + '_event.png'), event/np.max(event)*255)
        # cv.imwrite(os.path.join(tmp_path, format(i, '05d') + '_event_raw.png'), event_raw/np.max(event_raw)*255)
        if i >= 1:
            flows_hs.append(calcOpticalFlowFarneback(dist_surfaces[i-1], dist_surfaces[i], None, **hs_param))
            arrow_flow = vis_with_arrow((255-event_surfaces[i]).astype(np.uint8)*255, flows_hs[i-1])
            color_flow = flow_vis.flow_to_color(flows_hs[i-1], convert_to_bgr=False)
            cv.imwrite(os.path.join(save_path, 'hs_' + format(i, '05d') + '.png'), arrow_flow)
            cv.imwrite(os.path.join(save_path, 'hsc_' + format(i, '05d') + '.png'),
                       color_flow)

